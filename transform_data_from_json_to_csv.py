# -*- coding: utf-8 -*-

#
# Data transformation from JSON to CSV
# Asahi website (www.asahi.com)
#
# Omar Trejo
# August, 2016
#

import json
import pandas as pd

with open('asahi_mises_data.json') as json_file:
    data = json.load(json_file)

data = pd.read_json('asahi_mises_data.json', orient='index')

data.columns = [
    'Kinyubi',
    'Bangou',
    'Kigo',
    'Shogo',
    'Jugyoin',
    'Image',
    'Image_two',
    'Hipr',
    'Kana',
    'Tags',
    'Tags_HTML',
    'Tori_HTML'
]

data.to_csv('asahi_mises_data.csv', index=False, encoding='utf-8')
