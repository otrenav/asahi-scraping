/*
 * This should be done in the console after
 * the website has loaded succesfully
 */

// Create URL with data
var url = ('data:text/json;charset=utf8,' +
           encodeURIComponent(JSON.stringify(mises)));

// Open link with data
window.open(url, '_blank');

/*
 * Column names are:
 *
 * Word in HTML  Variable in HTML   Column name in extracted data
 * --------------------------------------------------------------
 * 0. Kinyubi    (misekinyubi)      Hidzuke
 * 1. Bangou     (misebangou)       Bangou
 * 2. Kigo       (misekigo)         Shizun
 * 3. Shogo      (miseshogo)        Gengo
 * 4. Jugyoin    (misejugyoin)      Shugyoshasu
 * 5. Image      (miseimg)          Image
 * 6. Image2     (miseimg2)         ImageTwo
 * 7. Hipr       (misehipr)         Hipr
 * 8. Kana       (misekana)         Kana
 * 9. Tags       (misetags)         Tags
 * 10. Tags HTML (misetags_html)    TagsHTML
 * 11. Tori HTML (misetori_html)    ToriHTML
 */

/*
 * If there are tags, use this the following code
 * (don't worry about this for now)
 */
// if (misetags.length > 0) {
//   for (k in misetags) {
//     if (TagArr[misetags[k]] != void 0) {
//       misetags_html += TagArr[misetags[k]][0];
//       if (misetags[k] != 12) {
//         misetori_html += TagArr[misetags[k]][2] + "、"
//       } else {
//         misetori_html += (mises[miseno][5] != '') ? 'その他（' + mises[miseno][5] + '）、' : 'その他、';
//       }
//     }
//   }
// }