
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Asahi website scrapping

- Omar Trejo
- Developed for Dr. Kensuke Teshima
- August, 2016

Scrapping was not actually necessary since there was a data object in the
website that I was able to download and just transform into CSV.

---

> "The best ideas are common property."
>
> —Seneca
